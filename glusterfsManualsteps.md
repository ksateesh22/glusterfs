### I have gotten the Following steps from 
https://docs.gluster.org/en/latest/Quick-Start-Guide/Quickstart/



### Add an additional disk to the VM and run the following command

``` bash
dmesg | grep SCSI 
```

### Run the following commands on all servers that we are setting up GlusterFs

```bash
mkfs.xfs -i size=512 /dev/sdc
mkdir -p /data/brick1
echo '/dev/sdc /data/brick1 xfs defaults 1 2' >> /etc/fstab
mount -a && mount 

```

### Install glusterfs server software

``` bash
yum install glusterfs-server
For ubuntu
apt-get update
apt-get install glusterfs-server
```

### Start gluster daemon and check status
``` bash
service glusterd start
service glusterd status
```
### Disable firewall on all of the servers for example

```bash
iptables -I INPUT -p all -s 10.0.1.4 -j ACCEPT
iptables -I INPUT -p all -s 10.0.1.5 -j ACCEPT
```

### Configure the trusted pool server 1 for example
``` bash 

gluster peer probe 10.0.1.5
gluster peer status
```
### Create volumes on all servers
``` bash
mkdir -p /data/brick1/gv0
```
### on server1 depending upon how servers we have create the volumes by using a command like the following
``` bash
gluster volume create gv0 replica 2 10.0.1.4:/data/brick1/gv0 10.0.1.5:/data/brick1/gv0
gluster volume start gv0
gluster volume info
gluster volume status
```

### install glusterfs client on the client machine
```bash
yum install glusterfs-client
```
### Create mount point on the client for example

``` bash
mount -t glusterfs 10.0.1.4:/gv0 /mnt
mount -t glusterfs 10.0.1.5:/gv0 /mnt
```
### Check if we are able to put files in the mount point and that is showing up on other servers

